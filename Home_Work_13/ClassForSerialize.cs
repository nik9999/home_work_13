﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Home_Work_13
{
    public class ClassForSerialize
    {
        public int I1 { get; set; }
        public int I2 { get; set; }
        public int I3 { get; set; }
        public int I4 { get; set; }
        public int I5 { get; set; }
    }


    //public static class MyExtensions
    //{
    //    public static ClassForSerialize Init(this ClassForSerialize obj)
    //    {
    //        Random rnd = new Random();

    //        obj.i1 = rnd.Next();
    //        obj.i2 = rnd.Next();
    //        obj.i3 = rnd.Next();
    //        obj.i4 = rnd.Next();
    //        obj.i5 = rnd.Next();

    //        obj.I1 = rnd.Next();
    //        obj.I2 = rnd.Next();
    //        obj.I3 = rnd.Next();
    //        obj.I4 = rnd.Next();
    //        obj.I5 = rnd.Next();

    //        return obj;
    //    }
    //}
}
