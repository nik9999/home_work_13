﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Home_Work_13
{
    public static class CsvConvert
    {
        public static string SerializeObject(object obj)
        {
            //var type = obj.GetType();

            var properties = getProperties(obj); //type.GetProperties(BindingFlags.Public | BindingFlags.Instance | BindingFlags.GetProperty | BindingFlags.SetProperty).ToList();

            var values = new List<string>();

            foreach (var p in properties)
            {
                var raw = p.GetValue(obj);
                var value = raw == null ? "" : raw.ToString();

                values.Add(value);
            }

            return string.Join(",", values.ToArray());
        }


        public static T DeserializeObject<T>(string sobj) where T : class, new()
        {
            var parts = sobj.Split(",");

            var datum = new T();
            var properties = getProperties(datum);

            int i = 0;
            foreach (var p in properties)
            {
                    var converter = TypeDescriptor.GetConverter(p.PropertyType);
                    var convertedvalue = converter.ConvertFrom(parts[i++]);

                    p.SetValue(datum, convertedvalue);

                }

                return datum;
        }

        private static List<PropertyInfo> getProperties(object obj)
        {
            return obj.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance | BindingFlags.GetProperty | BindingFlags.SetProperty).ToList();
        }
    }
}
