﻿using BenchmarkDotNet.Running;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Home_Work_13
{
    public class Program
    {
        public static void Main(string[] args)
        {

            BenchmarkRunner.Run<TheEasiestBenchmark>();

            Console.ReadLine();
        }
    }
}
