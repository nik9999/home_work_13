﻿using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Engines;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Home_Work_13
{
    public class TheEasiestBenchmark
    {
        ClassForSerialize _instance = new ClassForSerialize() {  I1 = 1, I2=2, I3 = 3, I4 = 4, I5=5 };

        [Benchmark(Description = "CSV Serialize", OperationsPerInvoke = 2)]
        public void SerializeCSV()
        {
            CsvConvert.SerializeObject(_instance);
        }

        [Benchmark(Description = "CSV Deserialize")]
        public void DeserializeCSV()
        {
            _instance = CsvConvert.DeserializeObject<ClassForSerialize>("1,2,3,4,5");
        }


        [Benchmark(Description = "Json Serialize")]
        public void SerializeJSON()
        {
            JsonConvert.SerializeObject(_instance);
        }

        [Benchmark(Description = "Json Deserialize")]
        public void DeserializeJSON()
        {
            _instance = JsonConvert.DeserializeObject<ClassForSerialize>("{\"I1\":1,\"I2\":2,\"I3\":3,\"I4\":4,\"I5\":5}");
        }

    }
}
