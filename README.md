123
``` ini

BenchmarkDotNet=v0.13.1, OS=Windows 10.0.19043.1387 (21H1/May2021Update)
Intel Core i5-2500 CPU 3.30GHz (Sandy Bridge), 1 CPU, 4 logical and 4 physical cores
.NET SDK=6.0.100
  [Host]     : .NET 6.0.0 (6.0.21.52210), X64 RyuJIT
  DefaultJob : .NET 6.0.0 (6.0.21.52210), X64 RyuJIT


```
|             Method |       Mean |    Error |   StdDev |
|------------------- |-----------:|---------:|---------:|
|    &#39;CSV Serialize&#39; |   467.7 ns |  3.43 ns |  3.21 ns |
|  &#39;CSV Deserialize&#39; | 2,422.9 ns | 17.15 ns | 16.04 ns |
|   &#39;Json Serialize&#39; |   958.4 ns |  6.07 ns |  5.68 ns |
| &#39;Json Deserialize&#39; | 1,951.9 ns | 11.44 ns | 10.15 ns |
